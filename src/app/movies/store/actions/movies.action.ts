import { Action } from "@ngrx/store";

export enum MoviesTypes {
  loadMovies = '[Movies] Load Movies',
  loadMoviesSuccess = '[Movies] Load Movies Success',
  loadMoviesFail ='[Movies] Load Movies Fail'
}


// action
class LoadMovies implements Action {
  readonly type = MoviesTypes.loadMovies;
  constructor(public payload: any) {};
}

class LoadMoviesSuccess implements Action {
  readonly type = MoviesTypes.loadMoviesSuccess;
  constructor(public payload: any) {};
}

class LoadMoviesFail implements Action {
  readonly type = MoviesTypes.loadMoviesFail;
  constructor(public payload: any) {};
}

// action creators
export const loadMovies = payload => new LoadMovies(payload);
export const loadMoviesSuccess = payload => new LoadMoviesSuccess(payload);
export const loadMoviesFail = payload => new LoadMoviesFail(payload);

// action types
export type MoviesAction = LoadMovies | LoadMoviesSuccess | LoadMoviesFail;


