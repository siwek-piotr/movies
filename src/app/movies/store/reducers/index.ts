import * as fromMovies from './movies.reducer'
import {ActionReducerMap, createFeatureSelector, createSelector, MemoizedSelector} from '@ngrx/store'
import { MoviesState } from "./movies.reducer";

export interface MoviesModuleState {
  movies: fromMovies.MoviesState;
}

export const reducers: ActionReducerMap<MoviesModuleState> = {
  movies: fromMovies.reducer,
};

export const getMoviesModuleState = createFeatureSelector<MoviesModuleState>('movies');

export const getMoviesState = createSelector(
  getMoviesModuleState,
  (state: MoviesModuleState) => state.movies
);

export const getMovies = createSelector(
  getMoviesState,
  fromMovies.getMovies,
)

export const getLoading = createSelector(
  getMoviesState,
  fromMovies.getMoviesLoading,
);

export const getLoaded= createSelector(
  getMoviesState,
  fromMovies.getMoviesLoaded,
);
