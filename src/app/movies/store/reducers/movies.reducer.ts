import { MoviesAction } from "../actions/movies.action";
import * as fromMovies from '../actions/movies.action'

export interface MoviesState {
  movies: any[];
  loaded: boolean;
  loading: boolean;
}

const moviesExample = [
  {id: 1, title: 'Power rangers'},
  {id: 2, title: 'Spider-man'},
  {id: 3, title: 'Batman'},
  {id: 4, title: 'Superman'}
  ];

const initialState = {
  movies: moviesExample,
  loaded: false,
  loading: false,
};

export const reducer = (state = initialState, action: MoviesAction): MoviesState => {
   switch(action.type) {
     case fromMovies.MoviesTypes.loadMovies:
       return {
         ...state,
        loading: true,
       };
     case fromMovies.MoviesTypes.loadMoviesSuccess:
       return {
         ...state,
         loading: false,
         loaded: true,
       };
     case fromMovies.MoviesTypes.loadMoviesFail:
       return {
         ...state,
         loading: false,
         loaded: true,
       };
     default:
       return state;
   }
};

export const getMoviesLoading = ({loading}: MoviesState) => loading;
export const getMoviesLoaded= ({loaded}: MoviesState) => loaded;
export const getMovies = ({movies}: MoviesState) => movies;
