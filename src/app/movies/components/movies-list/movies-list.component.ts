import { Component, OnInit } from '@angular/core';
import * as fromStore from '../../store';
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.scss']
})
export class MoviesListComponent implements OnInit {
  movies$: Observable<any[]>
  constructor(private store: Store<fromStore.MoviesModuleState>) { }

  ngOnInit() {
    this.movies$ = this.store.select(fromStore.getMovies)
  }

}
