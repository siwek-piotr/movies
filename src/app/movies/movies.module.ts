import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from "@ngrx/store";

import { reducers } from './store';
import { MoviesListComponent } from './components/movies-list/movies-list.component';
import { MoviesRoutingModule } from "./movies-routing.module";
import { MaterialModule}  from '../material.module';

@NgModule({
  declarations: [MoviesListComponent],
  imports: [
    CommonModule,
    MaterialModule,
    MoviesRoutingModule,
    StoreModule.forFeature('movies', reducers),
  ]
})
export class MoviesModule { }
