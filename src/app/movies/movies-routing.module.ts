import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";

import {MoviesListComponent} from "./components/movies-list/movies-list.component";

const routes: Routes = [
  {
    path: '',
    component: MoviesListComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class MoviesRoutingModule {}
