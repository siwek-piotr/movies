import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatListModule, MatRadioModule} from "@angular/material";

const importAndExport = [
  MatRadioModule,
  MatListModule,
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ...importAndExport
  ],
  exports: [...importAndExport]
})
export class MaterialModule { }
